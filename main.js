var ping = require('ping');
var mqtt = require('mqtt');

var serviceTopic = "pingu/telemetry";
var options = {
    "username": "staging:pingu",
    "password": "pingu",
    "clientId": "applet-pingu-home",
};
var client = mqtt.connect('mqtt://' + "node.pleq.io", options);

var connected = false;

connect = function() {
    if (!connected) {
        console.log(" # Succesfully connected to MQTT broker as " + JSON.stringify(options));
        connected = true;
    }
}

disconnect = function() {
    console.log(" # disconnected from MQTT broker as " + JSON.stringify(options));
    connected = false;
}

client.on('connect', connect)

client.on('disconnect', disconnect)

var hosts = ['192.168.0.210', '192.168.1.1', 'google.com', 'telenet.be'];

//var hosts = ["192.168.0.210"];

console.log("Starting... ");

//setInterval(function() {
    pingAllHosts()
//}, (60 * 1000) * 60 );


function pingAllHosts() {
    hosts.forEach(function(host) {
        // WARNING: -i 2 argument may not work in other platform like window 
        ping.promise.probe(host, {
            timeout: 10,
            extra: ["-c 4"],
        }).then(function(res) {

            if (res.alive == true) {
                //delete res.numeric_host;
                delete res.output;
                delete res.host;
                delete res.alive;

                var topic = serviceTopic + '/' + host.replace(/\./g, "_") + '/';

                Object.keys(res).forEach(function(key) {

                    var time = (new Date).getTime();

                    var message = {
                        id: key,
                        time: time,
                        value: res[key]
                    };

                    console.log("Publishing", topic + key.replace(/\./g, "_"), JSON.stringify(message));

                   	client.publish(topic + key.replace(/\./g, "_"), JSON.stringify(message), {
                        qos: 1
                    }, function(err) {
                        err ? console.log("err" + err) : null;
                    });

                });
            } else {
                console.log("alive", res.alive, res.host)
            }
        });
    });
}